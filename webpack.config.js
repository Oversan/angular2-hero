const path = require('path')

module.exports = {
    entry: path.join(__dirname, "src", "index.ts"),
    output: {
        path: path.join(__dirname, "public"),
        filename: "app.js"
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: ["", ".ts", ".tsx", ".js"]
    },
    module: {
        loaders: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    }
}